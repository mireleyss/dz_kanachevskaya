const burger = document.querySelector('.burger-menu')
const header_menu = document.querySelector('.header__menu')
const burger_img = document.querySelector('.menu-button')
const btn_orange = document.querySelector('.btn_orange') 
const body = document.body

burger.addEventListener('click', () => {
    header_menu.classList.toggle('active')
    burger.classList.toggle('active')
    burger_img.classList.toggle('active')
    btn_orange.classList.toggle('active')
    body.classList.toggle('stop_scroll')
})